import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		nowTime:'0:00',
		player:null,
		timer:0,
		sliderValue:0,
		meditation:{},
		member: {
			phone: ''
		},
		secretary:0,
		dateReport:0,
		noiseList:[],
		trainList:[],
		emotionInfo:{
			index:0,
			title:'',
			color:'',
			short:'',
			long:''
		},
		emotion_short:'',
		tiredInfo:{
			index:0,
			title:'',
			color:'',
			short:'',
			long:''
		},
		stressInfo:{
			index:0,
			title:'',
			color:'',
			short:'',
			long:''
		},
	},
	mutations: {
		SET_TRAIN(state, list) {
			state.trainList = list
		},
		SET_MEMBER(state, member) {
			state.member = member
		},
		SET_medita(state, medita) {
			state.meditation = medita
		},
		SET_sliderValue(state, sliderValue) {
			state.sliderValue = sliderValue
		},
		$setText(state, text){
			state.emotionInfo.short = text
		}
	}
})
export default store