# lime-echart-wordcloud 
- 为适配uniapp的lime-chart插件改写
- 基于 [echarts-wordcloud](https://github.com/ecomfe/echarts-wordcloud) 
- 依赖 [lime-echart](https://ext.dcloud.net.cn/plugin?id=4899)
如果您只想用词云但又不用echarts，推荐[词云](https://ext.dcloud.net.cn/plugin?id=6873)

## 示例
[lime echart](http://liangei.gitee.io/limeui/#/echart-example)

## 安装
- 1、在uniapp 插件市场 找到 [echarts](https://ext.dcloud.net.cn/plugin?id=4899) 导入
- 1、在uniapp 插件市场 找到 [echart-wordcloud](https://ext.dcloud.net.cn/plugin?id=8430) 导入


## 使用

```html
<l-echart ref="chartRef" @finished="init"></l-echart>
```


```js
// vue3 setup
import {ref} from 'vue'
import * as echarts from 'echarts'
import registerWordcloud from '@/uni_modules/lime-echart-wordcloud'
registerWordcloud(echarts)
const chartRef = ref(null)

const init = async () => {
	const chart = await chartRef.value.init(echarts)
	chart.setOption({
		series: [{
			type: 'wordCloud',
			// 由于许多平台无法创建离屏canvas，故在lime-chart里使用canvas标签，但由于生成的数据为异步。所以需要设置等待时间。
			wait: 100,
			shape: 'circle',
			// maskImage: maskImage,
			left: 'center',
			top: 'center',
			// width: '70%',
			// height: '80%',
			// right: null,
			// bottom: null,
			sizeRange: [12, 60],
			rotationRange: [-90, 90],
			rotationStep: 45,
			gridSize: 8,
			drawOutOfBound: false,
			layoutAnimation: true,
			textStyle: {
				fontFamily: 'sans-serif',
				fontWeight: 'bold',
				color: function() {
					// Random color
					return 'rgb(' + [
						Math.round(Math.random() * 160),
						Math.round(Math.random() * 160),
						Math.round(Math.random() * 160)
					].join(',') + ')';
				}
			},
			emphasis: {
				focus: 'self',
				textStyle: {
					shadowBlur: 10,
					shadowColor: '#333'
				}
			},

			data: [{
					name: '幸运转盘',
					value: 10
				},
				{
					name: '返回顶部',
					value: 85
				},
				{
					name: '心电图',
					value: 8
				},
				{
					name: '彩纸礼炮',
					value: 60
				},
				{
					name: 'LimeImage',
					value: 90
				},
				{
					name: '瀑布流',
					value: 60
				},
				{
					name: '颜色选择器',
					value: 80
				},
				{
					name: '图片剪刀',
					value: 40
				},
				{
					name: '图片压缩',
					value: 85
				},
				{
					name: '浮动气泡',
					value: 20
				},
				{
					name: '词云',
					value: 800
				},
				{
					name: 'LimeUi',
					value: 150
				},
				{
					name: 'LimeFileUtils',
					value: 150
				},
				{
					name: '文本省略',
					value: 80
				},
				{
					name: '小球飞入',
					value: 20
				},
				{
					name: '水波进度球',
					value: 10
				},
				{
					name: 'wordcloud',
					value: 250
				},
				{
					name: '电力',
					value: 120
				},
				{
					name: '翻滚文本动效',
					value: 30
				},
				{
					name: '徽标',
					value: 90
				},
				{
					name: '侧边栏',
					value: 70
				},
				{
					name: '浮动面板',
					value: 50
				},
				{
					name: 'indexes',
					value: 70
				},
				{
					name: 'resize',
					value: 110
				},
				{
					name: '拖拽排序',
					value: 30
				},
				{
					name: 'LimeShared',
					value: 90
				},
				{
					name: 'LimeIcon',
					value: 120
				},
				{
					name: '弹幕',
					value: 130
				},
				{
					name: 'pag',
					value: 60
				},
				{
					name: 'barcodegen',
					value: 80
				},
				{
					name: '录音管理',
					value: 50
				},
				{
					name: 'LimeCamera',
					value: 30
				},
				{
					name: 'LimeQrcode',
					value: 10
				},
				{
					name: 'echarts',
					value: 110
				},
				{
					name: 'LimeTheming',
					value: 30
				},
				{
					name: 'LimeSvga',
					value: 20
				},
				{
					name: 'dayuts',
					value: 90
				},
				{
					name: '进度环',
					value: 40
				},
				{
					name: 'chortcut',
					value: 70
				},
				{
					name: 'animateIt',
					value: 80
				},
				{
					name: 'LimeScan',
					value: 60
				},
				{
					name: 'LimeCall',
					value: 70
				},
				{
					name: '海报画板',
					value: 120
				},
				{
					name: 'clipboard',
					value: 80
				},
				{
					name: 'audio',
					value: 90
				},
				{
					name: '手写板',
					value: 100
				},
				{
					name: '防盗水印',
					value: 30
				},
				{
					name: 'loading',
					value: 180
				},
				{
					name: 'qrcodegen',
					value: 10
				},
				{
					name: 'tinycolor',
					value: 10
				}
			]
		}]
	});
}
```


## 打赏
如果你觉得本插件，解决了你的问题，赠人玫瑰，手留余香。  
![](https://testingcf.jsdelivr.net/gh/liangei/image@1.9/alipay.png)
![](https://testingcf.jsdelivr.net/gh/liangei/image@1.9/wpay.png)