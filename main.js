import App from './App'
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)
// uni.$u.config.unit = 'rpx'
import Vue from 'vue'
let mpShare = require('@/uni_modules/uview-ui/libs/mixin/mpShare.js');
Vue.mixin(mpShare)
import store from './store'
Vue.prototype.$store = store
Vue.config.productionTip = false

Vue.prototype.$navigateTo = function(url){
	uni.navigateTo({
		url
	})
}
Vue.prototype.$toast = function(str){
	uni.showToast({
		title:str,
		icon:'none'
	})
}
Vue.prototype.$model = function(str){
	uni.showModal({
		showCancel:false,
		title:'提示',
		content:str
	})
}
App.mpType = 'app'
const app = new Vue({
	store,
    ...App
})
require('./config/request.js')(app)
app.$mount()

