//获取数组连续的值最大的数组
function maxsequence(arr) {
	if (arr.length <= 2) {
		return [];
	}
	let result = [
		[]
	];
	let newarr = [];
	arr.reduce(function(previousValue, currentValue, currentIndex, array) {
		var tempArray = result[result.length - 1];
		if (previousValue + 1 === currentValue) {
			if (tempArray.length == 0) {
				tempArray.push(previousValue);
			}
			tempArray.push(currentValue);
		} else {
			if (tempArray.length >= 3) {
				result.push([]);
			} else {
				result[result.length - 1] = [];
			}
		}

		return currentValue;
	});
	if (result[result.length - 1].length == 0) {
		result.pop();
	}
	newarr = result[0];

	for (let i in result) {
		// if(result[i].length==newarr.length) newarr=result[i];
		if (result[i].length > newarr.length) newarr = result[i];
	}
	return newarr;
}

function toTotalArr(arr) {
	// var narr = new Array();
	// var test = new Array();
	var num = 1;
	var temp = "";
	let newArr = [];
	var size = arr.length;
	for (var i = 0; i < size; i++) {
		for (var j = i + 1; j < size; j++) {
			if (arr[i] == arr[j]) {
				arr.splice(j, 1)
				size--;
				j--;
				num++;
			}
		}
		// test[i] = num ;
		// narr[i] = arr[i]
		newArr.push({
			name: arr[i],
			value: num
		})
		num = 1;
	}
	return newArr;
	// console.log("size----");
	// console.log(arr);
}

function sortVla(a, b) {
	if (a.value < b.value) {
		return 1;
	} else if (a.value > b.value) {
		return -1;
	} else if (a.value == b.value) {
		return 0;
	}
}

function initReport(data, predata) {
	today = data || []
	preday = predata || []
	//情绪小时数据好中差占比均值
	let emotionHourPie = {
		daylist: [
			[],
			[],
			[]
		],
		data: {
			hig: 0,
			mid: 0,
			min: 0,
			mean: 0,
			total: 0
		},
		predata: {
			hig: 0,
			mid: 0,
			min: 0,
			mean: 0,
			total: 0
		}
	}
	let tiredHourPie = {
		daylist: [
			[],
			[],
			[]
		],
		data: {
			hig: 0,
			mid: 0,
			min: 0,
			mean: 0,
			total: 0
		},
		predata: {
			hig: 0,
			mid: 0,
			min: 0,
			mean: 0,
			total: 0
		}
	}
	let stressHourPie = {
		daylist: [
			[],
			[],
			[]
		],
		data: {
			hig: 0,
			mid: 0,
			min: 0,
			mean: 0,
			total: 0
		},
		predata: {
			hig: 0,
			mid: 0,
			min: 0,
			mean: 0,
			total: 0
		}
	}
	let datalen = 0;
	today.forEach(item => {
		if (item.emotion != null) {
			datalen += 1;
			emotionHourPie.data.mean += item.emotion;
			tiredHourPie.data.mean += item.tired;
			stressHourPie.data.mean += item.stress;
		}
		let emodayP = {
			total:0,
			hig: 0,
			mid: 0,
			min: 0,
		}
		let tirdayP = {
			total:0,
			hig: 0,
			mid: 0,
			min: 0,
		}
		let strdayP = {
			total:0,
			hig: 0,
			mid: 0,
			min: 0,
		}
		if(item.hourData){
			item.hourData.forEach(items => {
				if (items.emotion != null) {
					emotionHourPie.data.total += 1;
					tiredHourPie.data.total += 1;
					stressHourPie.data.total += 1;
					emotionHourPie.data[getPie(items.emotion)] += 1;
					tiredHourPie.data[getPie(items.tired)] += 1;
					stressHourPie.data[getPie(items.stress)] += 1;
					emodayP.total += 1;
					emodayP[getPie(items.emotion)] += 1;
					tirdayP[getPie(items.tired)] += 1;
					strdayP[getPie(items.stress)] += 1;
					tirdayP.total += 1;
					emodayP[getPie(items.emotion)] += 1;
					tirdayP[getPie(items.tired)] += 1;
					strdayP[getPie(items.stress)] += 1;
					strdayP.total += 1;
					emodayP[getPie(items.emotion)] += 1;
					tirdayP[getPie(items.tired)] += 1;
					strdayP[getPie(items.stress)] += 1;
				}
			})
		}
		// console.log(emotionHourPie.data);
		if(emodayP.total == 0){
			for(let i=0;i<3;i++){
				emotionHourPie.daylist[i].push(null)
				stressHourPie.daylist[i].push(null)
				tiredHourPie.daylist[i].push(null)
			}
		}else{
			// console.log(emotionHourPie.data.hig);
			let dp = ['hig','mid','min']
			for(let i=0;i<3;i++){
				emotionHourPie.daylist[i].push(emodayP[dp[i]])
				stressHourPie.daylist[i].push(strdayP[dp[i]])
				tiredHourPie.daylist[i].push(tirdayP[dp[i]])
			}
		}
	})

	emotionHourPie.data.mean = Math.round(emotionHourPie.data.mean / datalen);
	tiredHourPie.data.mean = Math.round(tiredHourPie.data.mean / datalen);
	stressHourPie.data.mean = Math.round(stressHourPie.data.mean / datalen);

	let percentAll = {
		emotion: [],
		tired: [],
		stress: [],
	};
	if (emotionHourPie.data.hig) {
		percentAll.emotion.push({
			total: emotionHourPie.data.total,
			value: emotionHourPie.data.hig,
			name: '心情愉悦',
			vlatype:'hig',
			itemStyle: {
				color: '#14b3f8'
			}
		})
	}
	if (emotionHourPie.data.mid) {
		percentAll.emotion.push({
			total: emotionHourPie.data.total,
			value: emotionHourPie.data.mid,
			vlatype:'mid',
			name: '心情一般',
			itemStyle: {
				color: '#fcc856'
			}
		})
	}
	if (emotionHourPie.data.min) {
		percentAll.emotion.push({
			total: emotionHourPie.data.total,
			value: emotionHourPie.data.min,
			vlatype:'min',
			name: '心情较差',
			itemStyle: {
				color: '#ff4d31'
			}
		})
	}
	if (stressHourPie.data.hig) {
		percentAll.stress.push({
			total: stressHourPie.data.total,
			value: stressHourPie.data.hig,
			vlatype:'hig',
			name: '压力较大',
			itemStyle: {
				color: '#ff4d31'
			}
		})
	}
	if (stressHourPie.data.mid) {
		percentAll.stress.push({
			total: stressHourPie.data.total,
			value: stressHourPie.data.mid,
			vlatype:'mid',
			name: '中等压力',
			itemStyle: {
				color: '#fcc856'
			}
		})
	}
	if (stressHourPie.data.min) {
		percentAll.stress.push({
			total: stressHourPie.data.total,
			value: stressHourPie.data.min,
			vlatype:'min',
			name: '放松',
			itemStyle: {
				color: '#14b3f8'
			}
		})
	}
	if (tiredHourPie.data.hig) {
		percentAll.tired.push({
			total: tiredHourPie.data.total,
			value: tiredHourPie.data.hig,
			vlatype:'hig',
			name: '疲惫',
			itemStyle: {
				color: '#ff4d31'
			}
		})
	}
	if (tiredHourPie.data.mid) {
		percentAll.tired.push({
			total: tiredHourPie.data.total,
			value: tiredHourPie.data.mid,
			vlatype:'mid',
			name: '精力一般',
			itemStyle: {
				color: '#fcc856'
			}
		})
	}
	if (tiredHourPie.data.min) {
		percentAll.tired.push({
			total: tiredHourPie.data.total,
			value: tiredHourPie.data.min,
			vlatype:'min',
			name: '精力充沛',
			itemStyle: {
				color: '#14b3f8'
			}
		})
	}
	percentAll.emotion = percentAll.emotion.sort(sortVla)
	percentAll.stress = percentAll.stress.sort(sortVla)
	percentAll.tired = percentAll.tired.sort(sortVla)
	let predatalen = 0;
	preday.forEach(item => {
		if (item.emotion != null) {
			predatalen += 1;
			emotionHourPie.predata.mean += item.emotion;
			tiredHourPie.predata.mean += item.tired;
			stressHourPie.predata.mean += item.stress;
		}
		if(!item.hourData){
			return;
		}
		item.hourData.forEach(items => {
			if (items.emotion != null) {
				emotionHourPie.predata.total += 1;
				tiredHourPie.predata.total += 1;
				stressHourPie.predata.total += 1;
				emotionHourPie.predata[getPie(items.emotion)] += 1;
				tiredHourPie.predata[getPie(items.tired)] += 1;
				stressHourPie.predata[getPie(items.stress)] += 1;
			}
		})
	})
	emotionHourPie.predata.mean = Math.round(emotionHourPie.predata.mean / predatalen);
	tiredHourPie.predata.mean = Math.round(tiredHourPie.predata.mean / predatalen);
	stressHourPie.predata.mean = Math.round(stressHourPie.predata.mean / predatalen);
	return {
		percentAll,
		emotionHourPie,
		tiredHourPie,
		stressHourPie
	}
}
function toDiff(pn,ln){
	let absNum = ln - pn;
	let pe = 0;
	if(absNum>0){
		pe = Math.round(absNum/pn*100);
		return {res:'up',resStr:'上升'+pe+'%'}
	}else if(absNum==0){
		return {res:'=',resStr:'持平'}
	}else{
		pe = Math.round(Math.abs(absNum)/pn*100);
		return {res:'dn',resStr:'降低'+pe+'%'}
	}
}
function sumToAvg(arr) {
	if (arr.length === 0) {
		return null;
	}
	let sum = arr.reduce((total, num) => total + num, 0);
	return Math.round(sum / arr.length);
};

function getPie(num) {
	if (num >= 0 && num <= 49) {
		return 'min'
	}
	if (num >= 50 && num <= 69) {
		return 'mid'
	}
	if (num >= 70 && num <= 100) {
		return 'hig'
	}
}
module.exports = {
	maxsequence,
	toTotalArr,
	initReport,
	toDiff
}