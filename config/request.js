module.exports = (vm) => {
    uni.$u.http.setConfig((config) => {
        config.baseURL = 'https://qingxusuanfa.com';
        return config
    })
	// 请求拦截
	uni.$u.http.interceptors.request.use((config) => {
	    config.data = config.data || {}
	    return config 
	}, config => {
	    return Promise.reject(config)
	})
	// 响应拦截
	uni.$u.http.interceptors.response.use((response) => {
		return response.data
	}, (response) => { 
		return Promise.reject(response)
	})
}
